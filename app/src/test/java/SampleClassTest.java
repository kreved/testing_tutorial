import org.junit.Test;

import accky.kreved.testing_tutorial.SampleClass;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

public class SampleClassTest {
    private SampleClass mSampleClass = new SampleClass();

    @Test
    public void testSampleSumMethod() throws Exception {
        int a = 3;
        int b = 12;
        assertEquals(mSampleClass.SampleSumMethod(a, b), 15);
    }

    @Test
    public void sameTestSampleSumMethod() throws Exception {
        int a = 3;
        int b = 12;
        assertEquals(mSampleClass.SampleSumMethod(a, b), 15);
    }

    @Test
    public void testWithAssertJ() throws Exception {
        int a = 10;
        int b = 12;
        assertThat(mSampleClass.SampleSumMethod(a, b)).isLessThan(23).isGreaterThan(21);
    }

    @Test
    public void testDoTheJob() throws Exception {
        assertThat(SampleClass.DoTheJob(7)).isEqualTo(8);
    }
}
