import android.widget.Button;
import android.widget.TextView;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.annotation.Config;
import org.robolectric.shadows.ShadowToast;

import accky.kreved.testing_tutorial.BuildConfig;
import accky.kreved.testing_tutorial.MainActivity;
import accky.kreved.testing_tutorial.R;

import static org.assertj.android.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk=21)
public class RobolectricTestExample {

    private MainActivity mActivity;

    @Before
    public void setUp() throws Exception {
        mActivity = Robolectric.setupActivity(MainActivity.class);
    }

    @Test
    public void testButtonClick() throws Exception {
        mActivity.findViewById(R.id.push_me_button).performClick();
        assertThat((TextView)mActivity.findViewById(R.id.txt)).hasText("true");
    }

    @Test
    public void testButtonClickClick() throws Exception {
        mActivity.findViewById(R.id.push_me_button).performClick();
        mActivity.findViewById(R.id.push_me_button).performClick();
        assertThat((TextView) mActivity.findViewById(R.id.txt)).hasText("false");
    }

    @Test
    public void testShadows() throws Exception {
        Button toastButton = (Button) mActivity.findViewById(R.id.toast_button);
        toastButton.performClick();
        assertThat(ShadowToast.getTextOfLatestToast()).isEqualTo(mActivity.getResources().getString(R.string.test_toast));
    }
}
