package accky.kreved.testing_tutorial;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = SomeModule.class)
public interface SampleComponent {
    void inject(MainActivity mainActivity);
}
