package accky.kreved.testing_tutorial;

import android.app.Application;

public class SuperApplication extends Application {

    public SampleComponent getComponent() {
        return mComponent;
    }

    public void setComponent(SampleComponent mComponent) {
        this.mComponent = mComponent;
    }

    private SampleComponent mComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        mComponent = DaggerSampleComponent.create();
//        mComponent = DaggerSuperApplication_TestSampleComponent.builder().testSomeModule(new TestSomeModule()).build();

    }
}
