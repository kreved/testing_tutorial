package accky.kreved.testing_tutorial;

import android.support.test.runner.AndroidJUnit4;
import android.test.ActivityInstrumentationTestCase2;

import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Singleton;

import dagger.Component;
import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

@RunWith(AndroidJUnit4.class)
public class DaggetTest extends ActivityInstrumentationTestCase2<MainActivity> {
    private final TestSampleComponent mComponent;

    @Module
    class TestSomeModule {
        @Provides
        @Singleton
        SampleInterface provideSampleInstance() {
            return mock(SampleClass.class);
        }
    }

    @Singleton @Component(modules = TestSomeModule.class)
    interface TestSampleComponent extends SampleComponent {}


    public DaggetTest(Class<MainActivity> activityClass) {
        super(activityClass);
        mComponent = DaggerDaggetTest_TestSampleComponent.builder().testSomeModule(new TestSomeModule()).build();
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mComponent.inject(getActivity());
    }

    @Test
    public void testTest() throws Exception {
        // TODO
    }
}
